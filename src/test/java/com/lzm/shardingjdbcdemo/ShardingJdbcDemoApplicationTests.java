package com.lzm.shardingjdbcdemo;

import com.lzm.shardingjdbcdemo.entity.Order;
import com.lzm.shardingjdbcdemo.entity.User;
import com.lzm.shardingjdbcdemo.mapper.OrderMapper;
import com.lzm.shardingjdbcdemo.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Random;

@SpringBootTest
class ShardingJdbcDemoApplicationTests {


    @Autowired
    private UserMapper userMapper;


    @Autowired
    private OrderMapper orderMapper;
    /**
     * 垂直分片：插入数据测试
     */
    @Test
    public void testInsertOrderAndUser(){

        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setUname("强哥"+i);
            userMapper.insert(user);

            Order order = new Order();
            order.setOrderNo(new Random().nextInt(100)+"ATGUIGU001"+new Random().nextInt(100));
            order.setUserId(user.getId());
            order.setAmount(new BigDecimal(i));
            orderMapper.insert(order);
        }


    }

    @Test
    @Transactional
    void contextLoads() {
        User user = new User();
        user.setUname("张三丰"+new Random().nextInt(100));
        List<User> users1 = userMapper.selectList(null);
        List<User> users2 = userMapper.selectList(null);
        userMapper.insert(user);
        List<User> users3 = userMapper.selectList(null);
    }



    @Test
    void contextLoads1() {
        List<User> users1 = userMapper.selectList(null);
        List<User> users2= userMapper.selectList(null);
    }
}
