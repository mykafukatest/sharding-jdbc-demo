package com.lzm.shardingjdbcdemo.controller;

import com.lzm.shardingjdbcdemo.entity.User;
import com.lzm.shardingjdbcdemo.mapper.UserMapper;
import com.lzm.shardingjdbcdemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/user")
@RestController
public class UserController {
    @Autowired
    private UserService userService;
    @RequestMapping("/allUsers")
    public List<User> allUsers(){
        return userService.list();
    }
}
