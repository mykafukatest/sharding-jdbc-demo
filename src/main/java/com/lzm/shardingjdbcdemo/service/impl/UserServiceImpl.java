package com.lzm.shardingjdbcdemo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzm.shardingjdbcdemo.entity.User;
import com.lzm.shardingjdbcdemo.mapper.UserMapper;
import com.lzm.shardingjdbcdemo.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
}
