package com.lzm.shardingjdbcdemo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzm.shardingjdbcdemo.entity.User;

public interface UserService extends IService<User> {
}
